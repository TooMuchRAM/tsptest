package tsp

import org.hexworks.zircon.api.CP437TilesetResources
import org.hexworks.zircon.api.Shapes
import org.hexworks.zircon.api.SwingApplications
import org.hexworks.zircon.api.application.AppConfig
import org.hexworks.zircon.api.color.ANSITileColor
import org.hexworks.zircon.api.color.TileColor
import org.hexworks.zircon.api.data.Position
import org.hexworks.zircon.api.data.Size
import org.hexworks.zircon.api.data.Tile
import org.hexworks.zircon.api.graphics.Symbols
import org.hexworks.zircon.api.grid.TileGrid
import org.hexworks.zircon.api.uievent.MouseEvent
import org.hexworks.zircon.api.uievent.MouseEventType
import org.hexworks.zircon.api.uievent.UIEventPhase
import org.hexworks.zircon.api.uievent.UIEventResponse
import kotlin.math.roundToInt


class CoordinateDrawer {

    private val tileGrid: TileGrid
    private val config: AppConfig = AppConfig.newBuilder()
            .withDefaultTileset(CP437TilesetResources.rexPaint8x8())
            .withTitle("Travelling Salesman Problem")
            .withSize(Size.create(125, 125))
            .withFpsLimit(60)
            .withDebugMode(false)
            .build()

    private val points = mutableListOf<LatLng>()
    private var startingPoint: LatLng? = null

    private var lines = mutableListOf<WonkyLine>()

    private var newPointListener: ((LatLng) -> Unit )?= null


    init {
        tileGrid = SwingApplications.startTileGrid(config)

        // listens to mouse events
        tileGrid.handleMouseEvents(MouseEventType.MOUSE_CLICKED) { event: MouseEvent, phase: UIEventPhase ->

            val coordinates = LatLng(event.position.x.toDouble(), event.position.y.toDouble())

            // If a listener exists, call it
            // Otherwise, execute the default action
            if (newPointListener != null) {
                newPointListener?.let { it(coordinates) }
            } else {
                addPoint(coordinates)
                redraw(TMRTSP.Optimisations.None)
            }


            // This was already there
            UIEventResponse.processed()
        }
    }

    fun addNewPointListener(listener: (LatLng) -> Unit) {
        newPointListener = listener
    }

    fun redraw(optimisations: TMRTSP.Optimisations) {
        //Recalculate the route
        if (startingPoint != null) {
            val tspAlgorithm = TMRTSP()
            // Draw black lines of the existing lines
            // This is to hide the existing lines
            // It's a hacky workaround, I know
            lines.forEach {
                val line = WonkyLine(it.from, it.to, it.position, ANSITileColor.BLACK)
                drawLine(line)
            }
            lines.clear()
            drawRoute(tspAlgorithm.selectBestRoute(startingPoint!!, getPointList(), optimisations))
        }
        // Redraw the points
        drawPoints()
    }

    fun addPoint(coordinates: LatLng) {
        // Add it to the list
        points.add(coordinates)
    }

    fun setStartingPoint(coordinates: LatLng) {
        startingPoint = coordinates
    }

    fun addPointList(coordinateList: List<LatLng>) {
        coordinateList.forEach { coordinate ->
            addPoint(coordinate)
        }
    }

    fun drawPoints() {
        points.forEach { coordinates ->
            drawPoint(coordinates)
        }
        if (startingPoint != null) {
            // Draw starting point
            drawPoint(startingPoint!!, ANSITileColor.YELLOW)
        }
    }

    fun getPointList(): List<LatLng> {
        return points.toList()
    }

    private fun drawPoint(point: LatLng, colour: ANSITileColor = ANSITileColor.RED) {
        tileGrid.draw(
                Shapes.buildFilledRectangle(
                        Position.zero(),
                        Size.create(2, 2)
                ).toTileGraphics(
                        Tile.newBuilder()
                                .withForegroundColor(colour)
                                .withCharacter(Symbols.BLOCK_SOLID)
                                .withBackgroundColor(TileColor.transparent())
                                .build(),
                        config.defaultTileset
                ),
                Position.create(point.latitude.roundToInt(), point.longitude.roundToInt())
        )
    }

    fun drawRoute(route: List<LatLng>) {
        route.forEachIndexed { i, routePoint ->
            if (i + 1 == route.count()) return
            val nextRoutePoint = route[i + 1]

            val upperleftcorner: LatLng = if (routePoint.latitude < nextRoutePoint.latitude) {
                if (routePoint.longitude < nextRoutePoint.longitude) {
                    LatLng(routePoint.latitude, routePoint.longitude)
                } else {
                    LatLng(routePoint.latitude, nextRoutePoint.longitude)
                }
            } else {
                if (routePoint.longitude < nextRoutePoint.longitude) {
                    LatLng(nextRoutePoint.latitude, routePoint.longitude)
                } else {
                    LatLng(nextRoutePoint.latitude, nextRoutePoint.longitude)
                }
            }
            val position = Position.create(
                    upperleftcorner.latitude.roundToInt(),
                    upperleftcorner.longitude.roundToInt()
            )
            val line = WonkyLine(
                    Position.create(routePoint.latitude.roundToInt(), routePoint.longitude.roundToInt()),
                    Position.create(nextRoutePoint.latitude.roundToInt(), nextRoutePoint.longitude.roundToInt()),
                    position
            )

            lines.add(line)

            drawLine(
                    line
            )

        }
    }

    private class WonkyLine(
            val from: Position,
            val to: Position,
            val position: Position,
            val colour: ANSITileColor = ANSITileColor.BLUE
    )

    private fun drawLine(line: WonkyLine) {
        tileGrid.draw(
                Shapes.buildLine(
                        line.from,
                        line.to
                ).toTileGraphics(
                        Tile.newBuilder()
                                .withForegroundColor(line.colour)
                                .withBackgroundColor(TileColor.transparent())
                                .withCharacter(Symbols.BLOCK_SPARSE)
                                .buildCharacterTile(),
                        config.defaultTileset
                ),
                line.position
        )
    }
}