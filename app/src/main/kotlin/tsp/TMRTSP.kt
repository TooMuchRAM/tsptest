package tsp

import kotlin.math.PI
import kotlin.math.abs
import kotlin.math.acos
import kotlin.math.sqrt

class TMRTSP {

    /**
     * Calculates the distance between two points
     * Based on ya boi Pythagoras' theorem
     * Did you know he was against eating meat?
     */
    private fun distance(point0: LatLng, point1: LatLng): Double {
        return sqrt(((point0.latitude - point1.latitude)*(point0.latitude - point1.latitude) + (point1.longitude - point0.longitude)*(point1.longitude - point0.longitude)))
    }

    private enum class CalculationMethod {
        Full, OnlyX, OnlyY
    }

    enum class Optimisations {
        None, Angle, FourNext
    }

    class Route (
        val points: List<LatLng>,
        val distance: Double
    )

    /**
     * Calculate the angle a line between two points makes with the X-axis
     */
    private fun pointToPointAngle(point0: LatLng, point1: LatLng): Double {
        // SOS*CAS*TOA
        //     ^^^
        val a = abs(point0.latitude-point1.latitude)
        val s = distance(point0, point1)
        // De Cosinus is de aanliggende zijde gedeeld door de schuine zijde
        return acos(a.div(s))
    }

    /**
     * Finds the nearest point in a list of points
     */
    private fun findNearestPoint(point: LatLng, inPointList: List<LatLng>, method: CalculationMethod = CalculationMethod.Full): LatLng {
        var nearestPoint = LatLng(0.0, 0.0)
        var nearestPointDistance: Double? = null
        inPointList.forEach { position ->
            if (point != position) {
                val distance = when (method) {
                    CalculationMethod.OnlyX -> {
                        abs(point.latitude - position.latitude)
                    }
                    CalculationMethod.OnlyY -> {
                        abs(point.longitude - position.longitude)
                    }
                    else -> {
                        distance(point, position)
                    }
                }

                if (nearestPointDistance == null) {
                    nearestPointDistance = distance
                    nearestPoint = position
                } else if(distance < nearestPointDistance!!) {
                    nearestPoint = position
                    nearestPointDistance = distance
                }
            }
        }
        return nearestPoint
    }

    /**
     * @return an array of Pairs, each containing a point b and the distance from point a (being the parameter point) to point b, ranked in ascending order
     */
    private fun findNearestPoints(point: LatLng, inPointList: List<LatLng>, method: CalculationMethod = CalculationMethod.Full): List<Pair<LatLng, Double>> {
        val furthestPoints = mutableListOf<Pair<LatLng, Double>>()
        furthestPoints.add(Pair(inPointList[0], distance(point, inPointList[0])))

        inPointList.withIndex().forEach { (i, inPointListItem) ->
            if (point != inPointListItem && i != 0) {
                val distance = when (method) {
                    CalculationMethod.OnlyX -> {
                        abs(point.latitude - inPointListItem.latitude)
                    }
                    CalculationMethod.OnlyY -> {
                        abs(point.longitude - inPointListItem.longitude)
                    }
                    else -> {
                        distance(point, inPointListItem)
                    }
                }

                // This part is responsible for putting the current inPointListItem in the right place in nearestPoints
                for((j, nearestPointItem) in furthestPoints.withIndex()){
                    val (_, nearestPointItemDistance) = nearestPointItem
                    if (nearestPointItemDistance < distance) {
                        furthestPoints.add(j, Pair(inPointListItem, distance))
                        break
                    } else if (j+1 == furthestPoints.count()) {
                        furthestPoints.add(Pair(inPointListItem, distance))
                        break
                    }
                }
            }
        }
        // For some reason, the points are ranked in descending order
        // I want them in ascending order, so reverse the nearestPoints
        return furthestPoints.reversed()
    }

    fun calculateRoute(startingPoint: LatLng, pointList: List<LatLng>, optimisation: Optimisations = Optimisations.None): Route {
        val tempPointList = pointList.toMutableList()
        val route = mutableListOf<LatLng>()

        var totaldistance = 0.0

        var currentpoint = startingPoint
        route.add(currentpoint)
        while(tempPointList.count() > 0) {

            val nextPoint: LatLng = if (optimisation == Optimisations.Angle) {

                val closestPoint = findNearestPoint(currentpoint, tempPointList, CalculationMethod.Full)
                val distanceToClosestPoint = distance(currentpoint, closestPoint)

                val closestPointX = findNearestPoint(currentpoint, tempPointList, CalculationMethod.OnlyX)
                val distanceToClosestPointX = distance(currentpoint, closestPointX)
                val closestPointXAngle = pointToPointAngle(currentpoint, closestPointX)

                val closestPointY = findNearestPoint(currentpoint, tempPointList, CalculationMethod.OnlyY)
                val distanceToClosestPointY = distance(currentpoint, closestPointY)
                val closestPointYAngle = pointToPointAngle(currentpoint, closestPointY)

                if (
                        closestPointXAngle > 0.9599311 /* 55 deg */ &&
                        distanceToClosestPointX < 1.3*distanceToClosestPoint &&
                        closestPointXAngle < 0.5*PI /* 90 deg */-closestPointYAngle
                ) {
                    closestPointX
                } else if (
                        closestPointYAngle < 0.6108652 /* 35 deg */ &&
                        distanceToClosestPointY < 1.3*distanceToClosestPoint &&
                        closestPointYAngle < 0.5*PI /* 90 deg */-closestPointXAngle
                ) {
                    closestPointY
                } else {
                    closestPoint
                }
            } else if (optimisation == Optimisations.FourNext) {
                val closestPoints = findNearestPoints(currentpoint, tempPointList)
                if (closestPoints.count() == 1) {
                    closestPoints[0].first
                } else {

                    val iterations = if (closestPoints.count() > 4) {
                        4
                    } else {
                        closestPoints.count()
                    }


                    var shortestRoute: Route? = null
                    print("\n")
                    for (i in 0 until iterations) {
                        if (closestPoints[i].second < 1.7*closestPoints[0].second) {
                            val tempPointListWithoutCurrentClosestPoint = tempPointList.toMutableList()
                            tempPointListWithoutCurrentClosestPoint.remove(closestPoints[i].first)
                            val closestPointItemRoute  = calculateRoute(closestPoints[i].first, tempPointListWithoutCurrentClosestPoint, Optimisations.None)
                            if (shortestRoute == null || shortestRoute.distance > closestPointItemRoute.distance) {
                                shortestRoute = closestPointItemRoute
                            }
                        }
                    }
                    shortestRoute!!.points[0]
                }
            } else {
                findNearestPoint(currentpoint, tempPointList, CalculationMethod.Full)
            }
            totaldistance += distance(currentpoint, nextPoint)

            route.add(nextPoint)
            tempPointList.remove(nextPoint)
            currentpoint = nextPoint
        }
        return Route(route.toList(), totaldistance)
    }

    fun selectBestRoute(startingPoint: LatLng, pointList: List<LatLng>, optimisations: Optimisations? = null): List<LatLng> {
        val routes = listOf(
                calculateRoute(startingPoint, pointList, Optimisations.None),
                calculateRoute(startingPoint, pointList, Optimisations.Angle),
                calculateRoute(startingPoint, pointList, Optimisations.FourNext)
        )

        var shortestRoute: Route? = null
        routes.forEach { route ->
            if (shortestRoute == null || route.distance < shortestRoute!!.distance) shortestRoute = route
        }

        println("Distance of normal route is ${routes[0].distance}")
        if (routes[0].distance == shortestRoute!!.distance) println("It is the shortest route")
        println("Distance of Angle route is ${routes[1].distance}")
        if (routes[1].distance == shortestRoute!!.distance) println("It is the shortest route")
        println("Distance of FourNext route is ${routes[2].distance}")
        if (routes[2].distance == shortestRoute!!.distance) println("It is the shortest route")

        if (optimisations == null) {
            return shortestRoute!!.points
        } else if (optimisations == Optimisations.Angle) {
            return routes[1].points
        } else if (optimisations == Optimisations.FourNext) {
            return routes[2].points
        } else {
            return routes[0].points
        }
    }
}