package tsp

class LatLng(var latitude: Double, var longitude: Double) {
    override fun toString(): String {
        return "($latitude, $longitude)"
    }
}