package tsp

fun main() {
    val coordinateDrawer = CoordinateDrawer()
    val coordinateDrawerWithOpt = CoordinateDrawer()
    val coordinateDrawerWithFNOpt = CoordinateDrawer()
    val newpointlistener = NewPointListener(listOf(coordinateDrawer, coordinateDrawerWithOpt, coordinateDrawerWithFNOpt))
    val points = listOf(
            LatLng(16.0, 14.0),
            LatLng(92.0, 68.0),
            LatLng(48.0, 12.0),
            LatLng(10.0, 24.0),
            LatLng(54.0, 54.0),
            LatLng(82.0, 66.0),
            LatLng(34.0, 36.0),
            LatLng(66.0, 80.0),
            LatLng(22.0, 74.0)
    )

    coordinateDrawer.addPointList(points)
    coordinateDrawerWithOpt.addPointList(points)
    coordinateDrawerWithFNOpt.addPointList(points)

    val startingPoint = LatLng(69.0, 69.0)
    coordinateDrawer.setStartingPoint(startingPoint)
    coordinateDrawerWithOpt.setStartingPoint(startingPoint)
    coordinateDrawerWithFNOpt.setStartingPoint(startingPoint)

    val tspAlgorithm = TMRTSP()
    val route = tspAlgorithm.selectBestRoute(startingPoint, points, TMRTSP.Optimisations.None)
    val routeWithOpt = tspAlgorithm.selectBestRoute(startingPoint, points, TMRTSP.Optimisations.Angle)
    val routeWithFNOpt = tspAlgorithm.selectBestRoute(startingPoint, points, TMRTSP.Optimisations.Angle)
    
    
    coordinateDrawer.addNewPointListener { newPoint -> 
        newpointlistener.newPoint(newPoint)
    }
    coordinateDrawerWithOpt.addNewPointListener { newPoint ->
        newpointlistener.newPoint(newPoint)
    }
    coordinateDrawerWithFNOpt.addNewPointListener { newPoint ->
        newpointlistener.newPoint(newPoint)
    }

    coordinateDrawer.drawRoute(route)
    coordinateDrawer.drawPoints()

    coordinateDrawerWithOpt.drawRoute(routeWithOpt)
    coordinateDrawerWithOpt.drawPoints()

    coordinateDrawerWithFNOpt.drawRoute(routeWithFNOpt)
    coordinateDrawerWithFNOpt.drawPoints()

}

class NewPointListener(private val coordinateDrawers: List<CoordinateDrawer>) {
    fun newPoint(newPoint: LatLng) {
        coordinateDrawers.forEach { drawer ->
            drawer.addPoint(newPoint)
            //drawer.redraw()
        }
        coordinateDrawers[0].redraw(TMRTSP.Optimisations.None)
        coordinateDrawers[1].redraw(TMRTSP.Optimisations.Angle)
        coordinateDrawers[2].redraw(TMRTSP.Optimisations.FourNext)
    }
}

